package com.test.demo.mapper;

import com.test.demo.domain.User;
import com.test.demo.model.UserDto;
import com.test.demo.model.UserRequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface UserMapper {
    UserDto toDto(User user);
    User toDomain(UserDto userDto);
    User toDomain(UserRequestDto requestDto);
    void mergeUsers(@MappingTarget User target, UserRequestDto source);
}
