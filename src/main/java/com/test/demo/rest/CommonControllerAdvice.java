package com.test.demo.rest;

import com.test.demo.enumeration.ErrorCode;
import com.test.demo.exception.AppException;
import com.test.demo.model.ExceptionDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@Component
@ControllerAdvice
@Slf4j
public class CommonControllerAdvice extends ResponseEntityExceptionHandler {
    @ExceptionHandler(AppException.class)
    public ResponseEntity<Object> handleAppException(AppException ex, ServletWebRequest request) {
        log.error("An exception occurred while processing " + request.getRequest().getMethod() + " on " + request.getRequest().getRequestURL(), ex);
        return handleExceptionInternal(
                ex,
                new ExceptionDto(ex.getCode(), ex.getMessage()),
                new HttpHeaders(),
                ex.getHttpStatus(),
                request
        );
    }

    public ResponseEntity<Object> handleUnexpectedException(Exception ex, ServletWebRequest request) {
        log.error("An unexpected exception occurred while processing " + request.getRequest().getMethod() + " on " + request.getRequest().getRequestURL(), ex);
        return handleExceptionInternal(
                ex,
                new ExceptionDto(ErrorCode.GENERIC_ERROR, "Generic error"),
                new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR,
                request
        );
    }

    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ServletWebRequest servletWebRequest = (ServletWebRequest) request;

        final String errors = ex.getBindingResult().getAllErrors().stream()
                .map(error -> ((FieldError) error).getField() + " " + error.getDefaultMessage())
                .collect(Collectors.joining(", "));
        log.error("An unexpected exception occurred while processing " + servletWebRequest.getRequest().getMethod() + " on " + servletWebRequest.getRequest().getRequestURL(), ex);
        return handleExceptionInternal(
                ex,
                new ExceptionDto(ErrorCode.ARGUMENTS_NOT_VALID, errors),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request
        );
    }

    public ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ServletWebRequest servletWebRequest = (ServletWebRequest) request;
        log.error("An exception occurred while processing " + servletWebRequest.getRequest().getMethod() + " on " + servletWebRequest.getRequest().getRequestURL(), ex);
        return handleExceptionInternal(
                ex,
                new ExceptionDto(ErrorCode.TYPE_MISMATCH, "Type mismatch error"),
                new HttpHeaders(),
                HttpStatus.BAD_REQUEST,
                request
        );
    }
}
