package com.test.demo.service.impl;

import com.test.demo.domain.User;
import com.test.demo.exception.UserNotFoundException;
import com.test.demo.mapper.UserMapper;
import com.test.demo.model.UserDto;
import com.test.demo.model.UserRequestDto;
import com.test.demo.repository.UserRepository;
import com.test.demo.service.UserService;
import org.springframework.transaction.annotation.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public Collection<UserDto> findAll() {
        log.info("Fetching all users");
        return userRepository
                .findAll()
                .stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto findById(Long id) {
        log.info("Fetching user with id {}", id);
        return userRepository
                .findById(id)
                .map(userMapper::toDto)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    @Transactional
    public UserDto createUser(UserRequestDto requestDto) {
        log.info("Creating new user {}", requestDto);

        User user = userMapper.toDomain(requestDto);

        userRepository.save(user);
        return userMapper.toDto(user);
    }

    @Override
    @Transactional
    public UserDto updateUser(UserRequestDto requestDto, Long id) {
        log.info("Updating user with id {}", id);

        User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));

        userMapper.mergeUsers(user, requestDto);
        return userMapper.toDto(user);
    }

    @Override
    @Transactional
    public void deleteUser(Long id) {
        log.info("Deleting user with id {}", id);

        if(!userRepository.existsById(id)) {
            throw new UserNotFoundException(id);
        } else
            userRepository.deleteById(id);
    }

}
