package com.test.demo.service;

import com.test.demo.model.UserDto;
import com.test.demo.model.UserRequestDto;

import java.util.Collection;
public interface UserService {
    Collection<UserDto> findAll();
    UserDto findById(Long id);
    UserDto createUser(UserRequestDto requestDto);
    UserDto updateUser(UserRequestDto requestDto, Long id);
    void deleteUser(Long id);
}
