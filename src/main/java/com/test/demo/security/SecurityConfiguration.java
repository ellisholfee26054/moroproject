package com.test.demo.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfiguration {
    private final PasswordEncoder passwordEncoder;

    @Bean
    public InMemoryUserDetailsManager userDetailsManager() {
        UserDetails admin = User.builder()
                .username("admin")
                .password(passwordEncoder.encode("password"))
                .roles("ADMIN")
                .build();

        return new InMemoryUserDetailsManager(admin);
    }

    @Bean
    public SecurityFilterChain configure(HttpSecurity http) throws Exception {
        return http
                .csrf().disable().cors().and().authorizeHttpRequests(aut -> {
                    aut.requestMatchers(HttpMethod.GET, "/api/v1/users/**", "/api/v1/users").permitAll();
                    aut.requestMatchers(HttpMethod.PUT, "/api/v1/users/**").permitAll();
                    aut.requestMatchers(HttpMethod.POST, "/api/v1/users").permitAll();
                    aut.requestMatchers(HttpMethod.DELETE, "/api/v1/user/**").hasRole("ADMIN");
                })
                //this code would be used if i had frontend with /login page to have an option tp log in or log out
//                .formLogin()
//                .loginPage("/login")
//                .and()
//                .logout().invalidateHttpSession(true)
//                .clearAuthentication(true)
//                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
//                .logoutSuccessUrl("/login")
//                .permitAll()
//                .and()
                .httpBasic(Customizer.withDefaults())
                .build();
    }


}
