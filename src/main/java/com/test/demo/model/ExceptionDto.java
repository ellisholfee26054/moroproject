package com.test.demo.model;

import com.test.demo.enumeration.ErrorCode;

public record ExceptionDto(ErrorCode code, String message) {
}
