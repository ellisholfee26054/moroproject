package com.test.demo.exception;

import com.test.demo.enumeration.ErrorCode;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public abstract class AppException extends RuntimeException {
    private final ErrorCode code;
    private final HttpStatus httpStatus;

    public AppException(String message, ErrorCode code, HttpStatus httpStatus) {
        super(message);
        this.code = code;
        this.httpStatus = httpStatus;
    }
}
