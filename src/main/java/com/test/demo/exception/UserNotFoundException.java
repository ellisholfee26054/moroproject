package com.test.demo.exception;

import com.test.demo.enumeration.ErrorCode;
import org.springframework.http.HttpStatus;

public class UserNotFoundException extends AppException{
    public UserNotFoundException(Long id) {
        super("User with id " + id + " not found", ErrorCode.USER_NOT_FOUND, HttpStatus.NOT_FOUND);
    }
}
