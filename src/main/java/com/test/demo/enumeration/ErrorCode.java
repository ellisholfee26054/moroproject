package com.test.demo.enumeration;

import lombok.Getter;

@Getter
public enum ErrorCode {
    GENERIC_ERROR("0000"),
    USER_NOT_FOUND("0001"),
    ARGUMENTS_NOT_VALID("0002"),
    TYPE_MISMATCH("0002");
    private final String code;

    ErrorCode(String code) {
        this.code=code;
    }
}
