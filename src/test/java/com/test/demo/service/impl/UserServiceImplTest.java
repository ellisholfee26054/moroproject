package com.test.demo.service.impl;

import com.test.demo.domain.User;
import com.test.demo.mapper.UserMapper;
import com.test.demo.model.UserDto;
import com.test.demo.model.UserRequestDto;
import com.test.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.assertj.core.api.WithAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RequiredArgsConstructor
class UserServiceImplTest implements WithAssertions {

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Spy
    private UserMapper userMapper;

    @Test
    void findAll() {
        //when
        //assert
        //verify
        User userTest = new User()
                .setId(1L)
                .setName("Joe")
                .setUsername("user")
                .setPassword("password");
        UserDto userDtoTest = new UserDto()
                .setId(1L)
                .setName("Joe");

        when(userRepository.findAll()).thenReturn(List.of(userTest));
        when(userMapper.toDto(userTest)).thenReturn(userDtoTest);

        Collection<UserDto> result = userService.findAll().stream().toList();

        assertThat(result).contains(userDtoTest);
        assertThat(result.size()).isEqualTo(1);

        verify(userRepository).findAll();
        verify(userMapper).toDto(userTest);
    }

    @Test
    void findById() {
        User userTest = new User()
                .setId(1L)
                .setName("Joe")
                .setUsername("user")
                .setPassword("password");
        UserDto userDtoTest = new UserDto()
                .setId(1L)
                .setName("Joe");

        when(userRepository.findById(1L)).thenReturn(Optional.of(userTest));
        when(userMapper.toDto(userTest)).thenReturn(userDtoTest);

        UserDto result = userService.findById(1L);

        assertThat(result.getId()).isEqualTo(userDtoTest.getId());

        verify(userRepository).findById(1L);
        verify(userMapper).toDto(userTest);

    }

    @Test
    void createUser() {
        User userTest = new User()
                .setId(1L)
                .setName("Joe")
                .setUsername("user")
                .setPassword("password");
        UserRequestDto userRequestTest = new UserRequestDto()
                .setName("Joe");
        UserDto userDtoTest = new UserDto()
                .setId(1L)
                .setName("Joe");

        when(userMapper.toDomain(userRequestTest)).thenReturn(userTest);
        when(userRepository.save(userTest)).thenReturn(userTest);
        when(userMapper.toDto(userTest)).thenReturn(userDtoTest);

        UserDto result = userService.createUser(userRequestTest);

        assertThat(result).isEqualTo(userDtoTest);

        verify(userMapper).toDto(userTest);
        verify(userRepository).save(userTest);
        verify(userMapper).toDomain(userRequestTest);
    }

    @Test
    void updateUser() {
        User userTest = new User()
                .setId(1L)
                .setName("Joe")
                .setUsername("user")
                .setPassword("password");
        UserRequestDto userRequestTest = new UserRequestDto()
                .setName("Joe");
        UserDto userDtoTest = new UserDto()
                .setId(1L)
                .setName("Joe");

        when(userRepository.findById(1L)).thenReturn(Optional.of(userTest));
        when(userMapper.toDto(userTest)).thenReturn(userDtoTest);


        UserDto result = userService.updateUser(userRequestTest, 1L);

        assertThat(userDtoTest).isEqualTo(result);

        verify(userRepository).findById(1L);
        verify(userMapper).mergeUsers(userTest, userRequestTest);
    }

    @Test
    void deleteUser() {
        when(userRepository.existsById(1L)).thenReturn(true);

        userService.deleteUser(1L);

        assertThat(userRepository.findById(1L)).isEqualTo(Optional.empty());

        verify(userRepository).deleteById(1L);
    }
}